#pragma once

#include "Core.h"

namespace Harmony {
	class HARMONY_API Application
	{
	public:
		Application();
		virtual ~Application();
		
		void Run();
	};

	// To be defined in client
	Application* CreateApplication();

}
