#pragma once

#ifdef HM_PLATFORM_WINDOWS
	#ifdef HM_BUILD_DLL
		#define HARMONY_API __declspec(dllexport)
	#else
		#define HARMONY_API __declspec(dllimport)
	#endif
#else
	#error Harmony only supports windows...
#endif