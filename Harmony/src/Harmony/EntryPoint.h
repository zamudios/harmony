#pragma once

#ifdef HM_PLATFORM_WINDOWS

extern Harmony::Application* Harmony::CreateApplication();

int main(int argc, char** argv)
{
	auto app = Harmony::CreateApplication();
	app->Run();
	delete app;
}

#endif